from setuptools import setup

import os

# Put here required packages
packages = ['Django<=1.6',]

if 'REDISCLOUD_URL' in os.environ and 'REDISCLOUD_PORT' in os.environ and 'REDISCLOUD_PASSWORD' in os.environ:
     packages.append('django-redis-cache')
     packages.append('hiredis')

setup(name='accounting',
      version='1.0',
      description='OpenShift App',
      author='smanikandan',
      author_email='smanirebel7@gmail.com',
      url='http://accounting-githubusersdata.rhcloud.com',
      install_requires=packages,
)

