from django.shortcuts import render
from django.views.generic import View
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse
from rest_framework import viewsets
from rest_framework import serializers
from django.views.generic import (
    ListView, DetailView,
    UpdateView, CreateView, DeleteView)

from .models import Company, Client, Project,ProjectTimeSpentDetails

# Create your views here.

class Home(View):

    def get(self,request):
        return render (request,'home.html')

class Dashboard(View):
    def get(self, request):
        no_of_clients =  Client.objects.all().count()
        no_of_projects = Project.objects.all().count()
        no_of_companys = Company.objects.all().count()
        total_cost = 0
        projects = Project.objects.all()
        for project in projects:
            total_cost += project.get_total_cost()
        context_dict = {'no_of_clients' : no_of_clients,'no_of_projects': no_of_projects,'no_of_companys':no_of_companys,'total_cost':total_cost}
        return render(request, 'dashboard.html',{'context_dict':context_dict})

class ClientListView(ListView):
    model = Client
    template_name = 'clients/client_list.html'

class ClientCreateView(CreateView):
    model = Client
    template_name = 'clients/client_form.html'
    fields = '__all__'
    success_url = reverse_lazy('client_list')

class ClientDetailView(DetailView):
    model = Client
    template_name = 'clients/client_detail.html'


class ClientUpdateView(UpdateView):
    model = Client
    fields = ['name', 'email', 'company']
    template_name = 'clients/client_form.html'

class ClientDeleteView(DeleteView):
    model = Client
    template_name = 'clients/client_confirm_delete.html'
    success_url = reverse_lazy('client_list')



class CompanyListView(ListView):
    model = Company
    template_name = 'companies/company_list.html'

class CompanyCreateView(CreateView):
    model = Company
    template_name = 'companies/company_form.html'
    fields = '__all__'
    success_url = reverse_lazy('company_list')

class CompanyDetailView(DetailView):
    model = Company
    template_name = 'companies/company_detail.html'

class CompanyUpdateView(UpdateView):
    model = Company
    fields = ['name', 'description']
    template_name = 'companies/company_form.html'

class CompanyDeleteView(DeleteView):
    model = Company
    template_name = 'companies/company_confirm_delete.html'
    success_url = reverse_lazy('company_list')




class ProjectListView(ListView):
    model = Project
    template_name = 'projects/project_list.html'


class ProjectCreateView(CreateView):
    model = Project
    template_name = 'projects/project_form.html'
    fields = '__all__'
    success_url = reverse_lazy('project_list')


class ProjectDetailView(DetailView):
    model = Project
    template_name = 'projects/project_detail.html'


class ProjectUpdateView(UpdateView):
    model = Project
    fields = ['name', 'client', 'start_date',
              'end_date', 'cost_per_hour']
    template_name = 'projects/project_form.html'


class ProjectDeleteView(DeleteView):
    model = Project
    template_name = 'projects/project_confirm_delete.html'
    success_url = reverse_lazy('project_list')


class ProjectTimeListView(ListView):
    model = ProjectTimeSpentDetails
    template_name = 'project_time_spent/project_time_spent_list.html'


class ProjectTimeCreateView(CreateView):
    model = ProjectTimeSpentDetails
    template_name = 'project_time_spent/project_time_spent_form.html'
    fields = ['project', 'started_at', 'ended_at']
    success_url = reverse_lazy('project_time_spent_list')


class ProjectTimeDetailView(DetailView):
    model = ProjectTimeSpentDetails
    context_object_name = "project_time_spent"
    template_name = \
        'project_time_spent/project_time_spent_detail.html'


class ProjectTimeUpdateView(UpdateView):
    model = ProjectTimeSpentDetails
    template_name = 'project_time_spent/project_time_spent_form.html'
    fields = ['project', 'started_at', 'ended_at']


class ProjectTimeDeleteView(DeleteView):
    model = ProjectTimeSpentDetails
    template_name = \
        'project_time_spent/project_time_spent_delete.html'
    success_url = reverse_lazy('project_time_spent_list')

class ReportsView(View):

    def __init__(self):
        pass

    def get(self,request):
        #import pdb;
        #pdb.set_trace()
        client_list = []
        client_details = Client.objects.all()
        for client in client_details:
            context_dict = {}
            context_dict['name'] = client.name
            context_dict['email'] = client.email
            context_dict['company_name'] = client.company.name
            context_dict['project_details'] = []
            total_hours = 0
            total_cost = 0
            no_of_projects = 0
            for project in client.project_set.all():
                hours = project.get_total_hours()
                cost = project.get_total_cost()
                total_hours += hours
                total_cost += cost
                context_dict['project_details'].append({project.id:project.name})
                no_of_projects +=1
            context_dict['total_hours'] = total_hours
            context_dict['total_cost'] = total_cost
            context_dict['no_of_projects'] = no_of_projects
            client_list.append(context_dict)

        project_list = []
        project_details = Project.objects.all()
        for project in project_details:
            project_context_dict = {}
            project_context_dict['name'] = project.name
            project_context_dict['client'] = project.client.name
            project_context_dict['start_date'] = project.start_date
            project_context_dict['end_date'] = project.end_date
            project_context_dict['cost_per_hour'] = project.cost_per_hour
            project_context_dict['total_hours'] = project.get_total_hours()
            project_context_dict['total_cost'] = project.get_total_cost
            project_list.append(project_context_dict)

        return render(request,'reports/reports_clients.html', {'context_dict':{'clients':client_list, 'projects':project_list }})

    def post(self,request):
        pass

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ('name', 'email',)

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Client.objects.all()
    serializer_class = UserSerializer